<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S01: Activities 1 and 2</title>
</head>
<body>

	<h1>Full Address</h1> 

	<p><?php echo getFullAddress('Philippines', 'Quezon City', 'Metro Manila', '555 Tandan Sora Ave'); ?></p>
	<p><?php echo getFullAddress('Philippines', 'Malolos City', 'Bulacan', '306 Matimbo'); ?></p>

	<h1>Letter-Based Grading</h1>

	<p>74 is Equivalent to <?php echo getLetterGrade(74); ?></p>
	<p>88 is Equivalent to <?php echo getLetterGrade(88); ?></p>
	<p>94 is Equivalent to <?php echo getLetterGrade(94); ?></p>


</body>
</html>